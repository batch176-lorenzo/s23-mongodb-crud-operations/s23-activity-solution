db.users.insertMany([
		{
			firstName: "Jay",
			lastName: "Contreras",
			email: "jaycontreras@mail.com",
			password: "jaycontreras",
			isAdmin: false
		},
		{
			firstName: "Jose",
			lastName: "Linao",
			email: "joselinao@mail.com",
			password: "linaojose",
			isAdmin: false
		},
		{
			firstName: "Zeppelin",
			lastName: "Tuyay",
			email: "zeppelintuyay32@mail.com",
			password: "zeptuyay321",
			isAdmin: false
		},
		{
			firstName: "Jason",
			lastName: "Astete",
			email: "astetejason@mail.com",
			password: "jasonastetehandsome",
			isAdmin: false
		},
		{
			firstName: "Allan",
			lastName: "Burdeos",
			email: "burdesosallan@mail.com",
			password: "burdeosallan",
			isAdmin: false
		}

	])


db.courses.insertMany([
		{
			name: "JavaScript 101",
			price: 5000,
			isActive: false
		},
		{
			name: "HTML 101",
			price: 2000,
			isActive: false
		},
		{
			name: "CSS 101",
			price: 2500,
			isActive: false
		}

	])

db.users.find({isAdmin: false})

db.users.updateOne(
	{
		firstName: "Jay"
	},
	{
		$set: {
			isAdmin: true
	}	
	}
	)
db.courses.updateOne(
	{},
	{
		$set: {
			isActive: true
		}
	})	

db.courses.deleteMany({isActive: false})